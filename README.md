# US-Power-Plant-Details (Springboot project)



## Project Objectives: 
This codebase was created to demonstrate a fully fledged full-stack application built with Spring boot + Angular 13 and MongoDB including plant operations, top plants and more.

## Prerequisites: 
**us-power-plant-details (spring boot project)** 

Your system needs to have the following minimum requirements to run a Spring Boot application:  
- **Java 8**  
- **Maven 3.2**  
- **MongoDB 4.4.3 Community [*should be UP and running*]**  
- **Clone project from [Git lab link](https://gitlab.com/wadile127/us-power-plant-details)**

#### Cloning & Starting Project Locally  

Please refer **US Power Plant Details Demo Project.docx** present inside **/resources/doc**

##### Server side (us-power-plant-details springboot project)  
a) Run/start up mongbo db service.  
b) I presume you are trying to compile the application and run it without using an IDE. I also presume you have maven installed and correctly added maven to your environment variable.  
- To install and add maven to environment variable visit [maven install](https://maven.apache.org/install.html)  if you are under a proxy check out add proxy to maven.   
- Navigate to the root of the project via command line and execute the command.  
**mvn spring-boot:run -Dspring-boot.run.profiles=dev  **
- The CLI will run application on the configured port.  

c) Open [Swagger-UI for APIs](http://localhost:8080/powerplant/adnoc/swagger-ui/index.html)  
d) Click on power-plant-details-controller  
e) Execute /powerplant/adnoc/plantDetails/processCSVFiles  

Once you execute above endpoint then mongo repo will be created and its ready to use by our applications.  

### Application Test report:  
Please refer **US Power Plant Details Demo Project.docx** present inside /resources/doc  

### Future enhancements:  
- CSV file uploading base on scheduler (using spring batch)  
- Need to apply rate limiting policy on spring boot application as its develop for non-authenticate users.
- Deploy the both the applications either on AWS/GCP or AZURE cloud.
- CI/CD integration along with container management using Kubernetes.
- Secure the application using APIGEE or Mulesoft gateway.
