package com.adnoc.powerplantdetails.repo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.adnoc.powerplantdetails.entity.PowerPlantEntity;

@SpringBootTest
public class PlantDetailsRepositoryTest {
	@Autowired
	PlantDetailsRepository plantDetailsRepository;
	
	@Test
	public void savePlantDetails() {
		PowerPlantEntity plantEntity = new PowerPlantEntity();
		plantEntity.setPlantAnnualNetGeneration(2000);
		plantEntity.setPlantLatitude(20000.00);
		plantEntity.setPlantLongitude(200000.20);
		plantEntity.setPlantName("Testing");
		plantEntity.setPlantStateAbbreviation("MW");
		plantEntity.setPlantTotalCombustionGenerationPercent("10%");
		plantDetailsRepository.save(plantEntity);
		
		List<PowerPlantEntity> expectedEntity = plantDetailsRepository.findByPlantStateAbbreviation("MW");
		String exptectedString = "MW";
		assertEquals(exptectedString, expectedEntity.get(0).getPlantStateAbbreviation());
		plantDetailsRepository.deleteById(expectedEntity.get(0).getId());
		
	}
	@Test
	public void delePlantDetails() {
		PowerPlantEntity plantEntity = new PowerPlantEntity();
		plantEntity.setPlantAnnualNetGeneration(2000);
		plantEntity.setPlantLatitude(20000.00);
		plantEntity.setPlantLongitude(200000.20);
		plantEntity.setPlantName("Testing");
		plantEntity.setPlantStateAbbreviation("AB");
		plantEntity.setPlantTotalCombustionGenerationPercent("10%");
		plantDetailsRepository.save(plantEntity);
		
		List<PowerPlantEntity> expectedEntity = plantDetailsRepository.findByPlantStateAbbreviation("AB");
		plantDetailsRepository.deleteById(expectedEntity.get(0).getId());
		expectedEntity = plantDetailsRepository.findByPlantStateAbbreviation("AB");
		assertEquals(0, expectedEntity.size());
	}

}
