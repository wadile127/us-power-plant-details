package com.adnoc.powerplantdetails.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.adnoc.powerplantdetails.entity.PowerPlantEntity;
import com.adnoc.powerplantdetails.entity.StateDetailsEntity;
import com.adnoc.powerplantdetails.model.PowerPlantDetails;
import com.adnoc.powerplantdetails.model.StateDetails;
import com.adnoc.powerplantdetails.repo.PlantDetailsRepository;
import com.adnoc.powerplantdetails.repo.StateDetailsRepository;

@SpringBootTest
public class PlantDetailsServiceTest {
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private PlantDetailsRepository plantDetailsRepository;
	
	@Autowired
	private StateDetailsRepository stateDetailsRepository;
	
	@Autowired
	private PlantDetailsService plantDetailsService;
	
	@Test
	public void readAllPlantDetails() {

		List<PowerPlantDetails> lstPowerPlantDetails = plantDetailsService.readAllPlantDetails();
		assertTrue(lstPowerPlantDetails.size() > 0);
	}
	
	@Test
	public void readStatesDetails() {
		List<StateDetails> lstStateDetails = plantDetailsService.readStatesDetails();
		assertTrue(lstStateDetails.size() > 0);
	}


	@Test
	public void readbyTopNAndState() {
		List<PowerPlantDetails> lstPowerPlantDetails = plantDetailsService.readbyTopNAndState(5, "AZ");
		assertEquals(5, lstPowerPlantDetails.size());
	}

}
