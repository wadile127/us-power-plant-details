package com.adnoc.powerplantdetails.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.adnoc.powerplantdetails.entity.PowerPlantEntity;


@Repository
public interface PlantDetailsRepository extends MongoRepository<PowerPlantEntity, String> {
	
	List<PowerPlantEntity> findByPlantStateAbbreviation(String stateName);
	
	Page<PowerPlantEntity> findAll(Pageable pageable);

}