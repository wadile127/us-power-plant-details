package com.adnoc.powerplantdetails.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.adnoc.powerplantdetails.entity.PowerPlantEntity;
import com.adnoc.powerplantdetails.entity.StateDetailsEntity;


@Repository
public interface StateDetailsRepository extends MongoRepository<StateDetailsEntity, String> {

}