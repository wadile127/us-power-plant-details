package com.adnoc.powerplantdetails.controller;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adnoc.powerplantdetails.model.PowerPlantDetails;
import com.adnoc.powerplantdetails.model.PowerPlantRequest;
import com.adnoc.powerplantdetails.model.StateDetails;
import com.adnoc.powerplantdetails.model.UpdatePlantRequest;
import com.adnoc.powerplantdetails.service.CSVFileProcessService;
import com.adnoc.powerplantdetails.service.PlantDetailsService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/plantDetails")
@Slf4j
@CrossOrigin
public class PowerPlantDetailsController {

	@Autowired
	private CSVFileProcessService csvFileProcessService;

	@Autowired
	PlantDetailsService plantDetailsService;


	@PostMapping(path = "/processCSVFiles")
	public ResponseEntity<String> processCSVFiles(@RequestHeader(required = false) String Authorization) {
		Instant start = Instant.now();
		csvFileProcessService.processCSVFile();
		Instant end = Instant.now();
		Duration timeElapsed = Duration.between(start, end);
		log.info("Total time taken by CSVFile processing is : " + timeElapsed.toMinutes() + " minutes");

		return new ResponseEntity<>("CSV File Imported", HttpStatus.CREATED);
	}

	
	@PostMapping(path = "/savePlant")
	public ResponseEntity<PowerPlantDetails> savePlant(@RequestBody PowerPlantRequest request, @RequestHeader(required = true) String Authorization) {
		log.info("Save plant details");
		PowerPlantDetails powerPlantDetails = plantDetailsService.savePowerPlant(request);
		return new ResponseEntity<>(powerPlantDetails, HttpStatus.CREATED);
	}

	@PostMapping(path = "/updatePlant/{plantId}")
	public ResponseEntity<PowerPlantDetails> updatePlant(@RequestBody(required = true) UpdatePlantRequest request, @PathVariable("plantId") String plantId, @RequestHeader(required = true) String Authorization) {
		log.info("Update plant details");
		PowerPlantDetails powerPlantDetails = plantDetailsService.updatePowerPlant(request, plantId);
		return new ResponseEntity<>(powerPlantDetails, HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping(path = "/removePlant/{plantId}")
	public ResponseEntity<String> removePlant(@PathVariable("plantId") String plantId, @RequestHeader(required = true) String Authorization) {
		log.info("Remove Power plant");
		plantDetailsService.removePowerPlant(plantId);
		return new ResponseEntity<>("REMOVED", HttpStatus.ACCEPTED);
	}
	
	@GetMapping(path = "/readStates")
	public ResponseEntity<List<StateDetails>> readStates() {
		log.info("Read all state details");
		List<StateDetails> lstStateDetails = plantDetailsService.readStatesDetails();
		return ResponseEntity.ok(lstStateDetails);
	}
	@GetMapping(path = "/readTopNState")
	public ResponseEntity<List<PowerPlantDetails>> readbyTopNAndState(@RequestParam(required = false) Integer topN, @RequestParam(required = false) String state) {
		log.info("Read by TopN And State");
		return ResponseEntity.ok(plantDetailsService.readbyTopNAndState(topN, state));
		
	}
	
}