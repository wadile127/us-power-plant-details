package com.adnoc.powerplantdetails.service;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.adnoc.powerplantdetails.entity.PowerPlantEntity;
import com.adnoc.powerplantdetails.entity.StateDetailsEntity;
import com.adnoc.powerplantdetails.model.NamedColumnBean;
import com.adnoc.powerplantdetails.model.NamedColumnBeanForState;
import com.adnoc.powerplantdetails.repo.PlantDetailsRepository;
import com.adnoc.powerplantdetails.repo.StateDetailsRepository;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.RFC4180Parser;
import com.opencsv.RFC4180ParserBuilder;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;

import lombok.extern.slf4j.Slf4j;



@Service
@Slf4j
public class CSVFileProcessServiceImpl implements CSVFileProcessService {
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	
	@Value("${config.localFilePath}")
	private String localFilePath;
	
	@Value("${config.localFilePathSates}")
	private String localFilePathForState;
	
	@Autowired
	PlantDetailsRepository plantDetailsRepository;
	
	@Autowired
	StateDetailsRepository stateDetailsRepository;


	@Override
	public void processCSVFile() {
		String strPath = localFilePath;
		Path path = Paths.get(strPath);
		parseAndInsert(path); // Parse and insert in MongoDb
		// For State CSV file loading
		path = Paths.get(localFilePathForState);
		parseAndInsertForState(path);
		
		//deleteFile(path); // Delete file once processing is done.
	}
	
	
	private void parseAndInsert(Path path) {
        try {
            log.info("parsing file {}", path.toAbsolutePath());
            List<NamedColumnBean> txList = parseCSV(path, NamedColumnBean.class); // Parsing using CSV file.
            // Sorting in decending order.
			List<NamedColumnBean> sortedList = txList.stream().sorted(Comparator.comparing(NamedColumnBean::getPlantAnnualNetGeneration).reversed())
					.collect(Collectors.toList());
            
			sortedList.stream().forEach(plantObj -> {
            	try {
            		PowerPlantEntity powerPlantEntity = new PowerPlantEntity();
            		powerPlantEntity.setPlantAnnualNetGeneration(plantObj.getPlantAnnualNetGeneration());
            		powerPlantEntity.setPlantLatitude(plantObj.getPlantLatitude());
            		powerPlantEntity.setPlantLongitude(plantObj.getPlantLongitude());
            		powerPlantEntity.setPlantName(plantObj.getPlantName());
            		powerPlantEntity.setPlantStateAbbreviation(plantObj.getPlantStateAbbreviation());
            		powerPlantEntity.setPlantTotalCombustionGenerationPercent(plantObj.getPlantTotalCombustionGenerationPercent());
            		powerPlantEntity.setPlantTotalNonhydroRenewablesGenPercent(plantObj.getPlantTotalNoncombustionGenPercent());
            		powerPlantEntity.setPlantTotalNonrenewablesGenePercent(plantObj.getPlantTotalNonrenewablesGenePercent());
            		powerPlantEntity.setPlantTotalRenewablesGenPer(plantObj.getPlantTotalRenewablesGenPer());
            		powerPlantEntity.setYear(plantObj.getYear());
            		powerPlantEntity.setPlantTotalNoncombustionGenPercent(plantObj.getPlantTotalNoncombustionGenPercent());
            		
   
            		plantDetailsRepository.save(powerPlantEntity);
            	}  catch (Exception e) {
                    log.error("error while inserting {} {}", plantObj.getPlantName(), plantObj.getPlantStateAbbreviation());
                }
            	
            });

        } catch (Exception e) {
            log.error("Error while inserting CSV records " + e.getMessage());
        }
    }

	private void parseAndInsertForState(Path path) {
        try {
            log.info("parsing file {}", path.toAbsolutePath());
            List<NamedColumnBeanForState> txList = parseCSVForStates(path, NamedColumnBeanForState.class); // Parsing using CSV file.
            
            
            txList.stream().forEach(stateObj -> {
            	try {
            		StateDetailsEntity stateDetailsEntity = new StateDetailsEntity();
            		stateDetailsEntity.setStateName(stateObj.getStateName());
            		stateDetailsEntity.setUsAbbreviation(stateObj.getUsAbbreviation());
            		stateDetailsRepository.save(stateDetailsEntity);
            	}  catch (Exception e) {
                    log.error("error while inserting {} {}", stateObj.getStateName(), stateObj.getUsAbbreviation());
                }
            	
            });

        } catch (Exception e) {
            log.error("Error while inserting CSV records " + e.getMessage());
        }
    }
	
    private List<NamedColumnBean> parseCSV(Path path, Class clazz) throws Exception {
		try (Reader reader = Files.newBufferedReader(path)) {
			final RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().build();
			final CSVReaderBuilder csvReaderBuilder = new CSVReaderBuilder(reader)
			    .withCSVParser(rfc4180Parser);
			CsvToBean cb = new CsvToBeanBuilder(csvReaderBuilder.build()).withType(clazz)
					.withIgnoreEmptyLine(true).withFieldAsNull(CSVReaderNullFieldIndicator.BOTH).
					//withEscapeChar('\\').
					withSeparator(',').
					withIgnoreLeadingWhiteSpace(true)
					//withSkipLines(1)
					.build();
			List<NamedColumnBean> csvList = new ArrayList<>();
			csvList.addAll(cb.parse());
			log.info("csv file parsed has size {}", csvList.size());
			return csvList;
		} catch (Exception e) {
			log.error("Error in parsing CS csv", e);
			throw e;
		}
		 
    }
    
    private List<NamedColumnBeanForState> parseCSVForStates(Path path, Class clazz) throws Exception {
		try (Reader reader = Files.newBufferedReader(path)) {
			final RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().build();
			final CSVReaderBuilder csvReaderBuilder = new CSVReaderBuilder(reader)
			    .withCSVParser(rfc4180Parser);
			CsvToBean cb = new CsvToBeanBuilder(csvReaderBuilder.build()).withType(clazz)
					.withIgnoreEmptyLine(true).withFieldAsNull(CSVReaderNullFieldIndicator.BOTH).
					//withEscapeChar('\\').
					withSeparator(',').
					withIgnoreLeadingWhiteSpace(true)
					//withSkipLines(1)
					.build();
			List<NamedColumnBeanForState> csvList = new ArrayList<>();
			csvList.addAll(cb.parse());
			log.info("csv file parsed has size {}", csvList.size());
			return csvList;
		} catch (Exception e) {
			log.error("Error in parsing CS csv", e);
			throw e;
		}
		 
    }
	
    
    private void deleteFile(Path path) {
        log.info("deleting file {}", path.toAbsolutePath());

        try {
            Files.delete(path);
        } catch (IOException e) {
            log.error("error deleting file {}", e.getMessage());
        }
    }
}
