package com.adnoc.powerplantdetails.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adnoc.powerplantdetails.entity.PowerPlantEntity;
import com.adnoc.powerplantdetails.entity.StateDetailsEntity;
import com.adnoc.powerplantdetails.exception.ResourceNotFoundException;
import com.adnoc.powerplantdetails.model.PowerPlantDetails;
import com.adnoc.powerplantdetails.model.PowerPlantRequest;
import com.adnoc.powerplantdetails.model.StateDetails;
import com.adnoc.powerplantdetails.model.UpdatePlantRequest;
import com.adnoc.powerplantdetails.repo.PlantDetailsRepository;
import com.adnoc.powerplantdetails.repo.StateDetailsRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PlantDetailsService {
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private PlantDetailsRepository plantDetailsRepository;
	
	@Autowired
	private StateDetailsRepository stateDetailsRepository;

	public List<PowerPlantDetails> readAllPlantDetails() {

		List<PowerPlantEntity> lstPowerPlantEntity = plantDetailsRepository.findAll();
		List<PowerPlantDetails> lstPowerPlantDetails = new ArrayList<PowerPlantDetails>();
		for (PowerPlantEntity powerPlantEntity : lstPowerPlantEntity) {
			PowerPlantDetails powerPlantDetails = new PowerPlantDetails();
			powerPlantDetails = convertToDto(powerPlantEntity);
			lstPowerPlantDetails.add(powerPlantDetails);
		}
		return lstPowerPlantDetails;
	}
	
	public List<StateDetails> readStatesDetails() {

		List<StateDetailsEntity> lstStateDetailsEntity = stateDetailsRepository.findAll();
		List<StateDetails> lstStateDetails = new ArrayList<StateDetails>();
		for (StateDetailsEntity stateDetailsEntity : lstStateDetailsEntity) {
			StateDetails stateDetails = new StateDetails();
			stateDetails = convertToDto(stateDetailsEntity);
			lstStateDetails.add(stateDetails);
		}
		return lstStateDetails;
	}

	public List<PowerPlantDetails> readTopNPlants(int topN) {
		
		List<PowerPlantEntity> lstPowerPlantEntity = plantDetailsRepository.findAll();
		List<PowerPlantDetails> lstPowerPlantDetails = new ArrayList<PowerPlantDetails>();
		for(PowerPlantEntity powerPlantEntity : lstPowerPlantEntity) {
			PowerPlantDetails powerPlantDetails = new PowerPlantDetails();
			powerPlantDetails = convertToDto(powerPlantEntity);
			lstPowerPlantDetails.add(powerPlantDetails);
		}
		List<PowerPlantDetails> topNPlantList = lstPowerPlantDetails.stream().limit(topN).collect(Collectors.toList());
		return topNPlantList;
	}

	public List<PowerPlantDetails> readAllPlantsByState(String stateName) {

		List<PowerPlantEntity> lstPowerPlantEntity = plantDetailsRepository.findByPlantStateAbbreviation(stateName);
		List<PowerPlantDetails> lstPowerPlantDetails = new ArrayList<PowerPlantDetails>();
		if (lstPowerPlantEntity.size() > 0) {
			for (PowerPlantEntity powerPlantEntity : lstPowerPlantEntity) {
				PowerPlantDetails powerPlantDetails = new PowerPlantDetails();
				powerPlantDetails = convertToDto(powerPlantEntity);
				lstPowerPlantDetails.add(powerPlantDetails);
			}
		} else {
			log.error("State Not found exception");
			throw new ResourceNotFoundException("State Not found exception ");
		}

		return lstPowerPlantDetails;
	}

	public PowerPlantDetails savePowerPlant(PowerPlantRequest request) {
		PowerPlantEntity powerPlantEntity = modelMapper.map(request, PowerPlantEntity.class);
		powerPlantEntity = plantDetailsRepository.save(powerPlantEntity);
		return convertToDto(powerPlantEntity);
	}

	public PowerPlantDetails updatePowerPlant(UpdatePlantRequest request, String plantId) {
		Optional<PowerPlantEntity> powerPlantEntity = plantDetailsRepository.findById(plantId);
		PowerPlantEntity powerPlantEntityUpdated = new PowerPlantEntity();
		if (powerPlantEntity.isPresent()) {
			powerPlantEntity.get()
					.setPlantAnnualNetGeneration(Optional.ofNullable(request.getPlantAnnualNetGeneration())
							.orElse(powerPlantEntity.get().getPlantAnnualNetGeneration()));
			powerPlantEntity.get().setPlantName(
					Optional.ofNullable(request.getPlantName()).orElse(powerPlantEntity.get().getPlantName()));
			powerPlantEntityUpdated = plantDetailsRepository.save(powerPlantEntity.get());
		}

		return convertToDto(powerPlantEntityUpdated);
	}

	public void removePowerPlant(String deletePowerPlant) {
		plantDetailsRepository.deleteById(deletePowerPlant);
	}

	private PowerPlantDetails convertToDto(PowerPlantEntity powerPlantEntity) {
		PowerPlantDetails powerPlantDetails = modelMapper.map(powerPlantEntity, PowerPlantDetails.class);
		return powerPlantDetails;
	}

	private StateDetails convertToDto(StateDetailsEntity stateDetailsEntity) {
		StateDetails stateDetails = modelMapper.map(stateDetailsEntity, StateDetails.class);
		return stateDetails;
	}
	private PowerPlantEntity convertToEntity(PowerPlantRequest powerPlantRequest) {
		PowerPlantEntity powerPlantEntity = modelMapper.map(powerPlantRequest, PowerPlantEntity.class);
		return powerPlantEntity;
	}
	
	public List<PowerPlantDetails> readbyTopNAndState(Integer topN, String stateName) {
		List<PowerPlantEntity> lstPowerPlantEntity = new LinkedList<>();
		if(stateName != null) {
			lstPowerPlantEntity = plantDetailsRepository.findByPlantStateAbbreviation(stateName);
		} else {
			lstPowerPlantEntity = plantDetailsRepository.findAll();
		}
		
		List<PowerPlantDetails> lstPowerPlantDetails = new ArrayList<PowerPlantDetails>();
		for(PowerPlantEntity powerPlantEntity : lstPowerPlantEntity) {
			PowerPlantDetails powerPlantDetails = new PowerPlantDetails();
			powerPlantDetails = convertToDto(powerPlantEntity);
			lstPowerPlantDetails.add(powerPlantDetails);
		}
		List<PowerPlantDetails> lstPowerPlantDetailsWithLimit = new LinkedList<PowerPlantDetails>();
		if(topN != null) {
			lstPowerPlantDetailsWithLimit = lstPowerPlantDetails.stream().limit(topN).collect(Collectors.toList());
			return lstPowerPlantDetailsWithLimit;
		}
		lstPowerPlantDetailsWithLimit = lstPowerPlantDetails.stream().limit(50).collect(Collectors.toList());
		return lstPowerPlantDetailsWithLimit;
	}

}
