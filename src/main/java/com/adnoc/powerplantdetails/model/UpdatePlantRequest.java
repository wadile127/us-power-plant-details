package com.adnoc.powerplantdetails.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * UpdatePlantRequest
 * @author Manoj Wadile
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdatePlantRequest {
	
    private String plantName;
    private Integer plantAnnualNetGeneration;
    

}
