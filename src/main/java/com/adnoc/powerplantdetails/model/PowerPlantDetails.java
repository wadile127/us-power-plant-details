package com.adnoc.powerplantdetails.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.opencsv.bean.CsvBindByName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Power plant details class
 * @author Manoj Wadile
 *
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class PowerPlantDetails {
    private String year;
    private String plantStateAbbreviation;
    private String plantName;
    private Double plantLatitude;
    private Double plantLongitude;
    private Integer plantAnnualNetGeneration;
    private String plantTotalNonrenewablesGenePercent;
    private String PlantTotalRenewablesGenPer;
    private String PlantTotalNonhydroRenewablesGenPercent;
    private String plantTotalCombustionGenerationPercent;
    private String PlantTotalNoncombustionGenPercent;
    private String id;
}
