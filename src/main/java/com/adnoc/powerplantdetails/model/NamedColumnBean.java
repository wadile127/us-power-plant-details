package com.adnoc.powerplantdetails.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CSV file NamedColuman Bean class
 * @author Manoj Wadile
 *
 */
public class NamedColumnBean extends CsvBean {
	//YEAR	PSTATABB	PNAME	ORISPL	LAT	LON	PLNGENAN	PLTNPR	PLTRPR	PLTHPR	PLCYPR	PLCNPR


    @CsvBindByName(column = "YEAR")
	//@CsvBindByPosition(position = 0)
    private String year;
    
    @CsvBindByName(column = "PSTATABB")
	//@CsvBindByPosition(position = 1)
    private String plantStateAbbreviation;
    
    @CsvBindByName(column = "PNAME")
    private String plantName;
    
    @CsvBindByName(column = "LAT")
    private Double plantLatitude;
    
    @CsvBindByName(column = "LON")
    private Double plantLongitude;
    
    @CsvBindByName(column = "PLNGENAN")
    private String plantAnnualNetGeneration;
    
    @CsvBindByName(column = "PLTNPR")
    private String plantTotalNonrenewablesGenePercent;
    
    @CsvBindByName(column = "PLTRPR")
    private String PlantTotalRenewablesGenPer ;
    
    @CsvBindByName(column = "PLTHPR")
    private String PlantTotalNonhydroRenewablesGenPercent;
    
    @CsvBindByName(column = "PLCYPR")
    private String plantTotalCombustionGenerationPercent ;
    
    @CsvBindByName(column = "PLCNPR")
    private String PlantTotalNoncombustionGenPercent;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getPlantStateAbbreviation() {
		return plantStateAbbreviation;
	}

	public void setPlantStateAbbreviation(String plantStateAbbreviation) {
		this.plantStateAbbreviation = plantStateAbbreviation;
	}

	public String getPlantName() {
		return plantName;
	}

	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	public Double getPlantLatitude() {
		return plantLatitude;
	}

	public void setPlantLatitude(Double plantLatitude) {
		this.plantLatitude = plantLatitude;
	}

	public Double getPlantLongitude() {
		return plantLongitude;
	}

	public void setPlantLongitude(Double plantLongitude) {
		this.plantLongitude = plantLongitude;
	}

	public Integer getPlantAnnualNetGeneration() {
		if(plantAnnualNetGeneration != null && !plantAnnualNetGeneration.isEmpty())  {
			return Integer.parseInt(plantAnnualNetGeneration.replace(",", "").replace("(", "").replace(")", ""));
		} else {
			return 0;
		}
		
	}

	public void setPlantAnnualNetGeneration(String plantAnnualNetGeneration) {
		this.plantAnnualNetGeneration = plantAnnualNetGeneration;
	}

	public String getPlantTotalNonrenewablesGenePercent() {
		return plantTotalNonrenewablesGenePercent;
	}

	public void setPlantTotalNonrenewablesGenePercent(String plantTotalNonrenewablesGenePercent) {
		this.plantTotalNonrenewablesGenePercent = plantTotalNonrenewablesGenePercent;
	}

	public String getPlantTotalRenewablesGenPer() {
		return PlantTotalRenewablesGenPer;
	}

	public void setPlantTotalRenewablesGenPer(String plantTotalRenewablesGenPer) {
		PlantTotalRenewablesGenPer = plantTotalRenewablesGenPer;
	}

	public String getPlantTotalNonhydroRenewablesGenPercent() {
		return PlantTotalNonhydroRenewablesGenPercent;
	}

	public void setPlantTotalNonhydroRenewablesGenPercent(String plantTotalNonhydroRenewablesGenPercent) {
		PlantTotalNonhydroRenewablesGenPercent = plantTotalNonhydroRenewablesGenPercent;
	}

	public String getPlantTotalCombustionGenerationPercent() {
		return plantTotalCombustionGenerationPercent;
	}

	public void setPlantTotalCombustionGenerationPercent(String plantTotalCombustionGenerationPercent) {
		this.plantTotalCombustionGenerationPercent = plantTotalCombustionGenerationPercent;
	}

	public String getPlantTotalNoncombustionGenPercent() {
		return PlantTotalNoncombustionGenPercent;
	}

	public void setPlantTotalNoncombustionGenPercent(String plantTotalNoncombustionGenPercent) {
		PlantTotalNoncombustionGenPercent = plantTotalNoncombustionGenPercent;
	}
    
}
