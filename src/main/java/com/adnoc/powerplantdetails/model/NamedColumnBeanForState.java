package com.adnoc.powerplantdetails.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CSV file NamedColuman Bean class for state CSV file
 * @author Manoj Wadile
 *
 */
public class NamedColumnBeanForState extends CsvBean {
	//stateName	usAbbreviation


    @CsvBindByName(column = "stateName")
	//@CsvBindByPosition(position = 0)
    private String stateName;
    
    @CsvBindByName(column = "usAbbreviation")
	//@CsvBindByPosition(position = 1)
    private String usAbbreviation;

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getUsAbbreviation() {
		return usAbbreviation;
	}

	public void setUsAbbreviation(String usAbbreviation) {
		this.usAbbreviation = usAbbreviation;
	}
    
    
}
