package com.adnoc.powerplantdetails.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Power plant detail class
 * @author Manoj Wadile
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PowerPlantDetail {

	private String year;
    private String plantStateAbbreviation;
    private String plantName;
    private Double plantLatitude;
    private Double plantLongitude;
    private Integer plantAnnualNetGeneration;
    private String plantTotalNonrenewablesGenePercent;
    private String PlantTotalRenewablesGenPer;
    private String PlantTotalNonhydroRenewablesGenPercent;
    private String plantTotalCombustionGenerationPercent;
    private String PlantTotalNoncombustionGenPercent;
}
