package com.adnoc.powerplantdetails.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * State detail class
 * @author Manoj Wadile
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

public class StateDetails {
	private String stateName;
	private String usAbbreviation;
}
