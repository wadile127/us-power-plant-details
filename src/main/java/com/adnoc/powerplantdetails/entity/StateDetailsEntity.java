package com.adnoc.powerplantdetails.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * State Details entity class
 * @author Manoj Wadile
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "USStateDetails")
public class StateDetailsEntity {
	@Id
    @GeneratedValue
    @JsonIgnore
	private String id;
	private String stateName;
	private String usAbbreviation;

}
