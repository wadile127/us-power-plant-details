package com.adnoc.powerplantdetails.exception;

/**
 * Custome exception for resource not found
 * @author Manoj Wadile
 *
 */
public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}